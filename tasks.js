// task 1
// (function () {
//     var a = b = 5;
// })()
// console.log(b); 3 // We declare a global variable 'b',which is accessible outside the function scope.
// console.log(a); ReferenceError: a is not defined // 'a' is function scoped and it is not accessible in the global scope

//task 1.1
// const f = () => {
//     var x = 3;
//     {
//         const x = 5;
//         console.log(x);
//     }
//     console.log(x);
// }
// f();
// Заменяме вторият 'var x' с 'let/const x',защото let/const са в блок скоуп(съществуват само в самия скоуп) и те не могат да пренапишат стойността на първия вар,както се случва при вар(вар е function scope,тоест втотия вар след изпълнение успява да замени стойността на първия).

//task 1 (advanced)  ??????
// const functions = [];
//     for (var i = 1; i <= 5; i++) {
//         functions.push(() => { console.log(i) });
//     }
//     functions.forEach(f => f());
// 1.
// 2.because var 'i' is in the global scope
// 3.Yes,the function inside the push method makes closure on the variable 'i'.
// 4.We can simply change 'var' with 'let' 
    

//shopping cart 

const product = {
    code: 'AF12C14', // sample code
    name: 'toothpaste', // sample name
    price: 4.95
};

function createShoppingCart() {
    const product = [];

    function addProduct(product1) {
        product.push(product1)
    }

    function containsProduct() {
        return product.length === 0 ? false : true;
    }

    function removeProduct(product1) {
        product.filter(el => el.code === product1.code )
    } // ?????

    function totalPrice() {
        return product.reduce((acc, el) => acc + el.price, 0)
    }
    return {
        addProduct,
        containsProduct,
        removeProduct,
        totalPrice,
        product
    }
}

const cart = createShoppingCart()
console.log(cart.containsProduct(product)); // false
cart.addProduct(product);
console.log(cart.containsProduct(product)); // true
cart.addProduct(product);
console.log(cart.totalPrice()) // 9.9
cart.removeProduct(product);
console.log(cart.totalPrice()) // 4.95
const cart2 = createShoppingCart();
console.log(cart2.totalPrice()) // 0

// Solving tasks
//1.   ??????????
// const map = (array, cb) => {};
// const array = [1,2,3,4,5]

// const myMap = (array, fn) => {
//     mapArray = []
//     for (const e of array) {
//         mapArray.push(fn(e))
//     }
//     return mapArray;
// }

// console.log(myMap(array, (e => e * 2)))

// //     const filter = (array, cb) => {};
// const myFilter = (array, fn) => {
//     filterArray = [];
//     for (const e of array) {
//         if (fn(e)) {
//         filterArray.push(e)
//     }
// }
//     return filterArray;
// }

// console.log(myFilter(array, (e => e > 3)));

    // const reduce = (array, fn, initial) => {};

// 2.
const orders = [
  { 
      item: 'beer', 
      quantity: 2, 
      price: 2.5,
      buyer: {
          name: 'pesho'
      } 
  },
  { 
      item: 'snacks', 
      quantity: 1, 
      price: 4.9,
      buyer: {
          name: 'gosho'
      } 
  },
  { 
      item: 'peanuts', 
      quantity: 4, 
      price: 1.8,
      buyer: {
          name: 'gosho'
      } 
  },
]
// 2.1
// const names = orders.map(el => el.item)
// console.log(names)
// 2.2
// const names = orders.filter(el => el.quantity >= 2).map(el => el.item)
// console.log(names);
// 2.3
// const buyers = orders.map(el => el.buyer.name);
// const buyersFilter = buyers.filter((el,index) => buyers.indexOf(el) === index)
// console.log(buyersFilter);
// 2.4
// const ordersByGosho = orders.filter(el => el.buyer.name === 'gosho').map(el => `${el.quantity} ${el.item}`)
// console.log(ordersByGosho);
// 2.5 
// const totalItems = orders.map(el => el.quantity).reduce((acc,el) => acc + el)
// console.log(totalItems);
// 2.6 
// const totalPrice = orders.map(el => el.price).reduce((acc,el) => acc + el)
// console.log(totalPrice);
// 2.7 

// const pricesForBuyer = orders.map(el => el.buyer)
// console.log(pricesForBuyer); 
// [ 
      //  { buyer: 'pesho', total: 5 } 
      //  { buyer: 'gosho', total: 12. 1 }, 
      // ]


//task 3
// const length = 4;
//     const numbers = [];
//     for (var i = 0; i < length; i++);{
//       numbers.push(i + 1);
//     }
//     numbers; // => [5] - the semicolon after the for statement,means that JS doesn't execute the block statement after each iteration.When the for loop stops at i=4 ,it executes the simple block statement that pushes (i + 1) to the numbers array. 

//task 4 
// const clothes = ['jacket', 't-shirt'];
//     clothes.length = 0;
//     clothes[0]; // => undefined - when the length property is set to 0,the array elements are deleted,e.g. array is empty.There is no first element,so we get undefined

// task 5
// function foo() {
//     let a = b = 0;
//     a++;
//     return a;
//   }
//   foo();
//   typeof a; // => undefined - variable 'a' exists only in the local scope
//   typeof b; // => number - variable 'b' is global variable and can be reached from the global scope

// task 6
// function arrayFromValue(item) {
//     return
//       [item];
//   }
//    arrayFromValue(10); // => when this piece of code :'[item]' is on the next row after the return statement,the function will return undefined.If item is on the same line with return statement,it will return '[10]'. 

// task 7
// const createCounter = () => {
//     let i  = 0;
//     function increment () {
//         return i++;
//     }
//     return increment;
// }
// const counter = createCounter()
//     console.log(counter()); // 0
//     console.log(counter()); // 1
//     console.log(counter()); // 2
//     console.log(counter()); // 3

// task 8 
// let i;
// for (i = 0; i < 3; i++) {
//   const log = () => {
//     console.log(i);
//   }
//   setTimeout(log, 100);
// }
// the output will be 3 times number 3.The for() iterates 3 times.On every iteration function log() is created and it captures 'i'.When the cycle completes,variable 'i' is 3.After that setTimeout function calls log(),which has closure on variable 'i'.That closure holds the value of 'i',which is 3.

//task 9
// myVar;   // => here the variable 'MyVar' is hoisted to the top,but its value is always 'undefined'.
//     // myConst; // => here we get an ReferenceError,because we can not access the variable  before initialization.It is in the temporal dead zone.
//     var myVar = 'value';
//     const myConst = 3.14;

// task 10
// 1 - const obj = {};
// 2 - const Car = {
//     model: 'Mercedes',
//     color: 'red',
// }
// const ElectricCar = Object.create(Car);
// 3 - function Car(model, color) {
//     this.model = model;
//     this.color = color;
// }

// var c1 = new Car('BMW', 'red');

//task 11 
// 1 - const arr = [];
// 2 - new Array(1,2,3,4)
// 3 - Array.from('array')

//task 12 
//we can create Promise with the keyword 'new'.
// const promise1 = new Promise((resolve, reject) => {
//     setTimeout(() => {
//       resolve('foo');
//     }, 300);
//   });
  
//   promise1.then((value) => {
//     console.log(value);
//     // expected output: "foo"
//   });
  
//   console.log(promise1);
  // expected output: [object Promise]

//task 13 
// Named functions can be declared in a statement or used in an expression.The name of the function is bound inside its body.We can use the name to have a function invoke itself, or to access its properties like any other object.
// named function declaration - function someFunc () {};
// named function expression - const someFunc = () => {};
// task 14
// The == operator is used for comparing two variables, but it ignores the datatype of variable.It does converts the types of the values if needed.
// const a = 10; const b = '10';
// console.log(a == b); // true
// the === operator is used for comparing two variables, but this operator also checks datatype and compares two values.
// const a = 10; const b = '10';
// console.log(a === b) // false

//task15
// 1 - .document.querySelector('id or class of the element') //for static collections
// 2 - .document.getElementsByTagName() dynamic collections


//task16
//There are 2 ways:The first is by writing JavaScript code in an <script> tags. The other is by including it into an external file.

//task17   ???????


//task 18 - 
// let ar = 2+5+'3' // shte bude string '73'.Purvo se izvurshva subiraneto na 4islata 2 i 5,sled tova 4isloto 7 + string ot 3,stava stringa '73'

// task 19 - Strict mode is helpful tool for the programmers to see errors,which JS doesn't always show.It can be enabled when you type 'use strict'.It is recommended to type it at the top of the file.Some examples of the special behaviour in strict mode are : 1.makes it impossible to accidentally create global variables. 2.Making assignments to non-writable properties or variables.3.Variety of syntax errors throws an errors. 

//task 20
// var y = 1;
//     if (function f() { }) {
//       y += typeof f;
//     }
//     console.log(y);// '1undefined' - funkciqta 'f' vrushta kato stoinost undefined.vuv if bloka pribavqme kum 1 stringa 'undefined' ot typeof operatora.'+' operatora prevrushta 1 vuv string i go obedinqva s 'undefined'.

//task 21 
// let arr = [1,2,3,'df0',5, {}, null];
// arr.length = 0;
// console.log(arr);

//task 22
// const output = (function (x) {
//     delete x;
//     return x;
//   })(0);
//   console.log(output);// returns 0 - the function is executed with the parameter '0' and it returns the same parameter.

//task  23
// const X = { foo: 1 };
//     const output = (function () {
//       delete X.foo;
//       return X.foo;
//     }
//     )();
//     console.log(output);// undefined - In the function the property 'foo' is deleted and then returned.The result from the IIFE is undefined 

// task 24  ???????
//nfe (named function expression)
// var foo = function bar() {
//     return 7;
//   };
//   typeof bar(); // We get an ReferenceError,because foo is function expression and evaluates to the number 7. Function bar() does not exist anymore.

//task 25 
// let name = 'Flavio'
//     let secondName = name;
//     name = 'Roger';
//     console.log(secondName);  //Flavio - zashtoto 'name' e primitiven tip i 'secondName' kopira negovata stoinost.
    

// task 26
// let car = {
//     color: 'yellow'
// }
// let anotherCar = car;
//       anotherCar.color = 'blue';
//       console.log(car); //obekt s property color:'blue' ,zashtoto object e ot referenten tip ,toest i dvete promenlivi so4at kum edin i susht obekt.
// console.log(car === anotherCar); // true - zashtoto 'car' i 'anotherCar' so4at kum edin i susht obekt./same by data type and value

//task 27
// let currencySymbol = "$";
//     function showMoney(amount) {
//       let currencySymbol = "€";
//         console.log(currencySymbol + amount);
//     }
//     showMoney("100");
    // Rezultatut e 100 evro,poneje promenlivata 'let' e function scoped.Vuv funkciqta se suzdava nova promenliva,koqto ne vzaimodeistva s promenlivata v globalniq scope.

// task 28
// let x = -1;
//     function xCounter() {
//           let x = 0;
//           return function() {
//               ++x;
//               return x;
//           };
//     }
//     console.log(x);   // -1 - tuk logvame 'x' koito se namira v globalniq scope i v slu4aq ne e zasegnat ot funkciqta xCounter
//     counter = xCounter();
//     console.log(counter());   // 1 -tuk logvame counter,koito e rezultatut ot funkciqta xCounter.Tazi fuknkciq vrushta kato stoinost druga funkciq,koqto ot svoq strana ni vrushta stoinostta na promenlivata 'x'.Predi da q vurne,tq pribava 1 kum stoinostta na'x'.Tuk funkciqta suzdava closure kum 'let x',za da moje po-kusno tq da bude dostupena.
//     console.log(counter());   // 2 - Sled kato izvikvame povtorno counter,stupkite se povtarqt,no stoinostta na promenlivata 'x' ve4e e ravna na 1.Sledovatelno rezultatut e 2.
//         console.log(x); // -1 - tuk otnovo logvame 'x',koqto e v globalniq scope i tq otnovo ostava nepromenena 
